"""Tests standard tap features using the built-in SDK tests library."""

from singer_sdk.testing import get_tap_test_class

from tap_connectwise.tap import TapConnectWise

SAMPLE_CONFIG = {
    "api_url": "",
    "publickey": "",
    "privatekey": "",
    "clientId": "",
    "companyId": "",
}


# Run standard built-in tap tests from the SDK:
TestTapConnectWise = get_tap_test_class(
    tap_class=TapConnectWise,
    config=SAMPLE_CONFIG,
)


# TODO: Create additional tests as appropriate for your tap.
