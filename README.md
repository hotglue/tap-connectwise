# tap-connectwise

`tap-connectwise` is a Singer tap for ConnectWise.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.

## Installation

Install from PyPi:

```bash
pipx install tap-connectwise
```

Install from GitHub:

```bash
pipx install git+https://gitlab.com/chauncywilson/tap-connectwise
```

## Configuration

### Accepted Config Options

## Capabilities

- `catalog`
- `state`
- `discover`
- `about`
- `stream-maps`
- `schema-flattening`
- `batch`

## Settings

| Setting              | Required | Default | Description                                                                                                                                                                                                                                              |
| :------------------- | :------: | :-----: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| api_url              |   True   |  None   | The url for the API service                                                                                                                                                                                                                              |
| clientId             |   True   |  None   | The client ID for the API                                                                                                                                                                                                                                |
| companyId            |   True   |  None   | The company ID for the API                                                                                                                                                                                                                               |
| publickey            |   True   |  None   | The public key for the API                                                                                                                                                                                                                               |
| privatekey           |   True   |  None   | The private key for the API                                                                                                                                                                                                                              |
| stream_maps          |  False   |  None   | Config object for stream maps capability. For more information check out [Stream Maps](https://sdk.meltano.com/en/latest/stream_maps.html).                                                                                                              |
| stream_map_config    |  False   |  None   | User-defined config values to be used within map expressions.                                                                                                                                                                                            |
| faker_config         |  False   |  None   | Config for the [`Faker`](https://faker.readthedocs.io/en/master/) instance variable `fake` used within map expressions. Only applicable if the plugin specifies `faker` as an addtional dependency (through the `singer-sdk` `faker` extra or directly). |
| flattening_enabled   |  False   |  None   | 'True' to enable schema flattening and automatically expand nested properties.                                                                                                                                                                           |
| flattening_max_depth |  False   |  None   | The max depth to flatten schemas.                                                                                                                                                                                                                        |
| batch_config         |  False   |  None   |                                                                                                                                                                                                                                                          |

A full list of supported settings and capabilities is available by running: `tap-connectwise --about`

## Supported Python Versions

- 3.8
- 3.9
- 3.10
- 3.11
- 3.12

A full list of supported settings and capabilities for this
tap is available by running:

```bash
tap-connectwise --about
```

### Configure using environment variables

This Singer tap will automatically import any environment variables within the working directory's
`.env` if the `--config=ENV` is provided, such that config values will be considered if a matching
environment variable is set either in the terminal context or in the `.env` file.

### Source Authentication and Authorization

<!--
Developer TODO: If your tap requires special access on the source system, or any special authentication requirements, provide those here.
-->

## Usage

You can easily run `tap-connectwise` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
tap-connectwise --version
tap-connectwise --help
tap-connectwise --config CONFIG --discover > ./catalog.json
```

## Developer Resources

Follow these instructions to contribute to this project.

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `tests` subfolder and
then run:

```bash
poetry run pytest
```

You can also test the `tap-connectwise` CLI interface directly using `poetry run`:

```bash
poetry run tap-connectwise --help
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._

<!--
Developer TODO:
Your project comes with a custom `meltano.yml` project file already created. Open the `meltano.yml` and follow any "TODO" items listed in
the file.
-->

Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-connectwise
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-connectwise --version
# OR run a test `elt` pipeline:
meltano elt tap-connectwise target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to
develop your own taps and targets.
