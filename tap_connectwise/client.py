"""REST client handling, including ConnectWiseStream base class."""

from __future__ import annotations

from typing import Any, Callable, Iterable

import requests
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.pagination import BaseAPIPaginator  # noqa: TCH002
from singer_sdk.streams import RESTStream
import typing as t
TPageToken = t.TypeVar("TPageToken")

_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]
if t.TYPE_CHECKING:
    from requests import Response
class ConnectWiseStream(RESTStream):
    """ConnectWise stream class."""
    def get_next_page_token(
        self,
        response: Response,
        previous_token: TPageToken | None,
    ) -> TPageToken | None:
        if not previous_token:
            return 2
        return previous_token+1
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return self.config.get("api_url","https://api-na.myconnectwise.net/v4_6_release/apis/3.0")

    records_jsonpath = "$[*]"  # Or override `parse_response`.

    # Set this value or override `get_new_paginator`.
    next_page_token_jsonpath = "$.next_page"  # noqa: S105

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object.

        Returns:
            An authenticator instance.
        """
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config["companyId"] + '+' + self.config["publickey"],
            password=self.config["privatekey"],
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed.

        Returns:
            A dictionary of HTTP headers.
        """
        headers = {
            "clientId": self.config["clientId"],
        }
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        # If not using an authenticator, you may also provide inline auth headers:
        # headers["Private-Token"] = self.config.get("auth_token")  # noqa: ERA001
        return headers

    

    def get_url_params(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ANN401
    ) -> dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization.

        Args:
            context: The stream context.
            next_page_token: The next page index or value.

        Returns:
            A dictionary of URL query parameters.
        """
        params: dict = {
            "pageSize": self.config.get("page_size", 1000), # 1,000 is the max page size
        }
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key
        return params

