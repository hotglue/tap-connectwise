"""Stream type classes for tap-connectwise."""

from __future__ import annotations

import typing as t

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_connectwise.client import ConnectWiseStream


class ActivitiesStream(ConnectWiseStream):
    name = "activities"
    path = "/sales/activities"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("type", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("company", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("contact", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("phoneNumber", th.StringType),
        th.Property("email", th.StringType),
        th.Property("status", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("opprotunity", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("ticket", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("summary", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("agreement", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("type", th.StringType),
            th.Property("chargeFirmFlag", th.BooleanType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("campaign", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("notes", th.StringType),
        th.Property("dateStart", th.DateTimeType),
        th.Property("dateEnd", th.DateTimeType),
        th.Property("assignedBy", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("dailyCapacity", th.IntegerType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("assignedTo", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            )),
        )),
        th.Property("scheduleStatus", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            )),
        )),
        th.Property("reminder", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            )),
        )),
        th.Property("where", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            )),
        )),
        th.Property("notifyFlag", th.BooleanType),
        th.Property("mobileGuid", th.StringType),
        th.Property("_info", th.ObjectType(
            th.Property("additionalProp1", th.StringType),
            th.Property("additionalProp2", th.StringType),
            th.Property("additionalProp3", th.StringType),
        )),
        th.Property("customFields", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("caption", th.StringType),
            th.Property("type", th.StringType),
            th.Property("entryMethod", th.StringType),
            th.Property("numberOfDecimals", th.IntegerType),
            th.Property("value", th.ObjectType())
        )),
    ).to_dict()


class ProjectStream(ConnectWiseStream):
    """Define custom stream."""

    name = "projects"
    path = "/project/projects"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("actualEnd", th.DateTimeType),
        th.Property("actualHours", th.NumberType),
        th.Property("actualStart", th.DateTimeType),
        th.Property("agreement", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("type", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billExpenses", th.StringType, description="Type of Bill Expenses"),
        th.Property("billingAmount", th.NumberType),
        th.Property("billingAttention", th.StringType),
        th.Property("billingMethod", th.StringType),
        th.Property("billingRateType", th.StringType),
        th.Property("billingTerms", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billProducts", th.StringType),
        th.Property("billProjectAfterClosedFlag", th.BooleanType),
        th.Property("billTime", th.StringType),
        th.Property("billToCompany", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billToContact", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billToSite", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billUnapprovedTimeAndExpense", th.BooleanType),
        th.Property("board", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("budgetAnalysis", th.StringType),
        th.Property("budgetFlag", th.BooleanType),
        th.Property("budgetHours", th.NumberType),
        th.Property("company", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("contact", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("currency", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("symbol", th.StringType),
            th.Property("decimalSeperator", th.StringType),
            th.Property("numberOfDecimals", th.IntegerType),
            th.Property("thousandsSeperator", th.StringType),
            th.Property("negativeParenthesesFlag", th.BooleanType),
            th.Property("displaySymbolFlag", th.BooleanType),
            th.Property("currencyIdentifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("downpayment", th.NumberType),
        th.Property("estimatedEnd", th.DateTimeType),
        th.Property("percentComplete", th.NumberType),
        th.Property("estimatedExpenseRevenue", th.NumberType),
        th.Property("estimatedHours", th.NumberType),
        th.Property("estimatedProductRevenue", th.NumberType),
        th.Property("estimatedStart", th.DateTimeType),
        th.Property("estimatedTimeRevenue", th.NumberType),
        th.Property("expenseApprover", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("includeDependenciesFlag", th.BooleanType),
        th.Property("includeEstimatesFlag", th.BooleanType),
        th.Property("location", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("department", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("manager", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("type", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("name", th.StringType),
        th.Property("opportunity", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("projectTemplateId", th.IntegerType),
        th.Property("restrictDownPaymentFlag", th.BooleanType),
        th.Property("scheduledEnd", th.DateTimeType),
        th.Property("scheduledHours", th.NumberType),
        th.Property("scheduledStart", th.DateTimeType),
        th.Property("shipToCompany", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("shipToContact", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("shipToSite", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("site", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("status", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("closedFlag", th.BooleanType),
        th.Property("timeApprover", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("type", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("billingStartDate", th.DateTimeType),
        th.Property("poAmount", th.NumberType),
        th.Property("estimatedTimeCost", th.NumberType),
        th.Property("estimatedExpenseCost", th.NumberType),
        th.Property("estimatedProductCost", th.NumberType),
        th.Property("taxCode", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("companyLocation", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("_info", th.ObjectType(
            th.Property("additionalProp1", th.StringType),
            th.Property("additionalProp2", th.StringType),
            th.Property("additionalProp3", th.StringType),
        ), description="Additional Properties"),
        th.Property("customFields", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("caption", th.StringType),
            th.Property("type", th.StringType),
            th.Property("entryMethod", th.StringType),
            th.Property("numberOfDecimals", th.IntegerType),
            th.Property("value", th.ObjectType())
        ))
    ).to_dict()


class TimeEntryStream(ConnectWiseStream):
    """Define custom stream."""

    name = "Time Entries"
    path = "/time/entries"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("company", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("chargeToId", th.IntegerType),
        th.Property("chargeToType", th.StringType),
        th.Property("member", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("locationId", th.IntegerType),
        th.Property("businessUnitId", th.IntegerType),
        th.Property("workType", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("workRole", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("agreement", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("type", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("timeStart", th.DateTimeType),
        th.Property("timeEnd", th.DateTimeType),
        th.Property("hoursDeduct", th.NumberType),
        th.Property("actualHours", th.NumberType),
        th.Property("billableOption", th.StringType),
        th.Property("notes", th.StringType),
        th.Property("internalNotes", th.StringType),
        th.Property("addToDetailDescriptionFlag", th.BooleanType),
        th.Property("addToInternalAnalysisFlag", th.BooleanType),
        th.Property("addToResolutionFlag", th.BooleanType),
        th.Property("emailResourceFlag", th.BooleanType),
        th.Property("emailContactFlag", th.BooleanType),
        th.Property("emailCcFlag", th.BooleanType),
        th.Property("emailCc", th.StringType),
        th.Property("hoursBilled", th.NumberType),
        th.Property("invoiceHours", th.NumberType),
        th.Property("enteredBy", th.StringType),
        th.Property("dateEntered", th.DateTimeType),
        th.Property("invoice", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("identifier", th.StringType),
            th.Property("billingType", th.StringType),
            th.Property("applyToType", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("mobileGuid", th.StringType),
        th.Property("hourlyRate", th.NumberType),
        th.Property("overageRate", th.NumberType),
        th.Property("agreementHours", th.NumberType),
        th.Property("agreementAmount", th.NumberType),
        th.Property("timeSheet", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("status", th.StringType),
        th.Property("ticket", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("summary", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("project", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("phase", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("_info", th.ObjectType(
                th.Property("additionalProp1", th.StringType),
                th.Property("additionalProp2", th.StringType),
                th.Property("additionalProp3", th.StringType),
            ), description="Additional Properties"),
        )),
        th.Property("_info", th.ObjectType(
            th.Property("additionalProp1", th.StringType),
            th.Property("additionalProp2", th.StringType),
            th.Property("additionalProp3", th.StringType),
        ), description="Additional Properties"),
        th.Property("customFields", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("caption", th.StringType),
            th.Property("type", th.StringType),
            th.Property("entryMethod", th.StringType),
            th.Property("numberOfDecimals", th.IntegerType),
            th.Property("value", th.ObjectType())
        ))
    ).to_dict()
