"""ConnectWise entry point."""

from __future__ import annotations

from tap_connectwise.tap import TapConnectWise

TapConnectWise.cli()
