"""ConnectWise tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_connectwise import streams
streams.ActivitiesStream
streams.ProjectStream
streams.TimeEntryStream


class TapConnectWise(Tap):
    """ConnectWise tap class."""

    name = "tap-connectwise"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_url",
            th.StringType,
            required=True,
            description="The url for the API service",
        ),
        th.Property(
            "clientId",
            th.StringType,
            required=True,
            secret=True,
            description="The client ID for the API",
        ),
        th.Property(
            "companyId",
            th.StringType,
            required=True,
            description="The company ID for the API",
        ),
        th.Property(
            "publickey",
            th.StringType,
            required=True,
            secret=True,
            description="The public key for the API",
        ),
        th.Property(
            "privatekey",
            th.StringType,
            required=True,
            secret=True,
            description="The private key for the API",
        )).to_dict()


    def discover_streams(self) -> list[streams.ConnectWiseStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.ActivitiesStream(self),
            streams.ProjectStream(self),
            streams.TimeEntryStream(self),
        ]


if __name__ == "__main__":
    TapConnectWise.cli()
